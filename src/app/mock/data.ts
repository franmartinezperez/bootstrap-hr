import { Patient } from "../models/patient"
import { Reviews } from "../models/reviews"

function randomDate(start, end) {
  return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()))
}

export const FAKE_PATIENTS: Patient[] = [
  { identifier: 1, name: "Pepe", surname: "adasd", birthDate: randomDate(new Date(1985, 0, 1), new Date()), numberOfRevisions: 45 },
  { identifier: 2, name: "Juan", surname: "adasd", birthDate: randomDate(new Date(1985, 0, 1), new Date()), numberOfRevisions: 89 },
  { identifier: 3, name: "Lithium", surname: "adasd", birthDate: randomDate(new Date(1985, 0, 1), new Date()), numberOfRevisions: 13 },
  { identifier: 4, name: "Beryllium", surname: "adasd", birthDate: randomDate(new Date(1985, 0, 1), new Date()), numberOfRevisions: 24 },
  { identifier: 5, name: "Boron", surname: "adasd", birthDate: randomDate(new Date(1985, 0, 1), new Date()), numberOfRevisions: 67 },
  { identifier: 6, name: "Carbon", surname: "adasd", birthDate: randomDate(new Date(1985, 0, 1), new Date()), numberOfRevisions: 20 },
  { identifier: 7, name: "Nitrogen", surname: "adasd", birthDate: randomDate(new Date(1985, 0, 1), new Date()), numberOfRevisions: 90 },
  { identifier: 8, name: "Oxygen", surname: "adasd", birthDate: randomDate(new Date(1985, 0, 1), new Date()), numberOfRevisions: 58 },
  { identifier: 9, name: "Fluorine", surname: "adasd", birthDate: randomDate(new Date(1985, 0, 1), new Date()), numberOfRevisions: 55 },
  { identifier: 10, name: "Neon", surname: "adasd", birthDate: randomDate(new Date(1985, 0, 1), new Date()), numberOfRevisions: 70 },
]

export const FAKE_REVIEWS: Reviews[] = [
  { number: 1, reviewDate: randomDate(new Date(1985, 0, 1), new Date()), doctor: "Rhoda Ross", diagnosis: "Bronquitis" },
  { number: 2, reviewDate: randomDate(new Date(1985, 0, 1), new Date()), doctor: "Alice Medina", diagnosis: "Resfriado" },
  { number: 3, reviewDate: randomDate(new Date(1985, 0, 1), new Date()), doctor: "Sadie Floyd", diagnosis: "Migrañas" },
  { number: 4, reviewDate: randomDate(new Date(1985, 0, 1), new Date()), doctor: "Herman Thomas", diagnosis: "Resfriado" },
  { number: 5, reviewDate: randomDate(new Date(1985, 0, 1), new Date()), doctor: "Bernard Aguilar", diagnosis: "Resfriado" },
  { number: 6, reviewDate: randomDate(new Date(1985, 0, 1), new Date()), doctor: "Ellen Mathis", diagnosis: "Resfriado" }
]
