export class Path {

   // util
   public static readonly BACKSLASH = "/"

   // empty
   public static readonly DEFAULT = ""
   public static readonly ANY = "**"

   // patients
   public static readonly MODULE_PATIENTS = "patients"
   public static readonly SCREEN_PATIENTS_LIST_TABLE = "list"
   public static readonly SCREEN_PATIENTS_DETAIL = "detail"
}
