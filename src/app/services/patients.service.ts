import { Injectable } from "@angular/core"
import { Patient } from "../models/patient"
import { FAKE_PATIENTS } from "../mock/data"

@Injectable()
export class PatientsService {
  private _patients: Patient[] = []

  constructor() {
    if (localStorage.getItem("test.patients")) {
      this._patients = JSON.parse(localStorage.getItem("test.patients"))
    } else {
      this.patients = FAKE_PATIENTS
    }
  }

  set patients(value: Patient[]) {
    this._patients = value
    localStorage.setItem("test.patients", JSON.stringify(this.patients))
  }

  get patients(): Patient[] {
    return this._patients
  }

  // TODO
  addPatient(patient: Patient) {
    this._patients.push(patient)
    localStorage.setItem("test.patients", JSON.stringify(this._patients))
  }
}
