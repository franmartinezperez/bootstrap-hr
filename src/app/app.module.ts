import { BrowserModule } from "@angular/platform-browser"
import { NgModule } from "@angular/core"
import {
  MatButtonModule,
  MatDialogModule,
  MatSortModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCardModule } from "@angular/material"
import { MatTableModule } from "@angular/material/table"
import { AppRoutingModule } from "./app-routing.module"
import { AppComponent } from "./app.component"
import { BrowserAnimationsModule, NoopAnimationsModule } from "@angular/platform-browser/animations"
import { PatientsService } from "./services/patients.service"
import { FormsModule, ReactiveFormsModule } from "@angular/forms"
import { HttpClientModule } from "@angular/common/http"

const modules = [
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSortModule,
  MatDialogModule,
  MatTableModule,
  MatCardModule
]

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    modules
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    PatientsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
