import { NgModule } from "@angular/core"
import { RouterModule, Routes } from "@angular/router"
import { Path } from "./configs/path"


const routes: Routes = [
  {
    path: Path.MODULE_PATIENTS,
    loadChildren: () => import("./ui/patients/patients.module").then(m => m.PatientModule)
  },
  {
    path: Path.DEFAULT,
    redirectTo: Path.MODULE_PATIENTS,
    pathMatch: "full"
  }]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
