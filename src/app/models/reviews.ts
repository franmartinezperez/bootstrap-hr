export interface Reviews {
  number: number
  reviewDate: Date
  doctor: string
  diagnosis: string
}
