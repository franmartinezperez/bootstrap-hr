import { TestBed, async, ComponentFixture } from "@angular/core/testing"
import { RouterTestingModule } from "@angular/router/testing"
import {
  MatButtonModule,
  MatDialogModule,
  MatSortModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatTableModule,
  MatCardModule
} from "@angular/material"
import { PatientListComponent } from "./patient-list.component"
import { ReactiveFormsModule, FormsModule } from "@angular/forms"
import { PatientsService } from "src/app/services/patients.service"
import { BrowserAnimationsModule } from "@angular/platform-browser/animations"

describe("AppComponent", () => {
  let component: PatientListComponent
  let fixture: ComponentFixture<PatientListComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatButtonModule,
        MatDialogModule,
        MatSortModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatTableModule,
        MatCardModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
      ],
      declarations: [
        PatientListComponent
      ],
      providers: [
        PatientsService
      ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientListComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it("should create", () => {
    expect(component).toBeTruthy()
  })

})
