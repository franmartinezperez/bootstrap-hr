import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core"
import { MatSort } from "@angular/material/sort"
import { MatTableDataSource, MatTable } from "@angular/material/table"
import { MatDialog } from "@angular/material"
import { Router } from "@angular/router"
import { AddPatientsComponent } from "../add-patients/add-patients.component"
import { Patient } from "src/app/models/patient"
import { PatientsService } from "src/app/services/patients.service"
import { Path } from "src/app/configs/path"

/**
 * @title Table with sorting
 */
@Component({
  selector: "patient-list",
  styleUrls: ["./patient-list.component.scss"],
  templateUrl: "./patient-list.component.html",
})
export class PatientListComponent implements OnInit {
  displayedColumns: string[] = ["identifier", "name", "surname", "birthDate", "numberOfRevisions", "actions"]
  dataSource: any

  @ViewChild(MatSort, { static: true }) sort: MatSort
  @ViewChild("table", { static: true }) table: MatTable<Patient>

  constructor(
    private patientsService: PatientsService,
    private matDialog: MatDialog,
    private changeDetectorRefs: ChangeDetectorRef,
    private rout: Router
  ) {

  }

  ngOnInit() {
    this.refresh()
  }

  // TODO
  addPatient() {
    this.matDialog.afterAllClosed.subscribe(data => this.refresh())
    this.matDialog.open(AddPatientsComponent, {
      width: "350px",
      height: "100%",
    })
  }

  refresh() {
    this.dataSource = new MatTableDataSource<Patient>(this.patientsService.patients)
    this.dataSource.sort = this.sort
    this.changeDetectorRefs.detectChanges()
  }

  rowClick(row: Patient) {
    localStorage.setItem("selectedPatient", JSON.stringify(row))
    this.rout.navigateByUrl(Path.MODULE_PATIENTS + Path.BACKSLASH + Path.SCREEN_PATIENTS_DETAIL)
  }
}
