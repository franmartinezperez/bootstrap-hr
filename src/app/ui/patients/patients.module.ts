import { NgModule } from "@angular/core"
import {
  MatButtonModule,
  MatDialogModule,
  MatSortModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatTableModule,
  MatCardModule } from "@angular/material"
import { PatientsRoutingModule } from "./patients-routing.module"
import { PatientListComponent } from "./patient-list/patient-list.component"
import { PatientDetailComponent } from "./patient-detail/patient-detail.component"
import { AddPatientsComponent } from "./add-patients/add-patients.component"
import { CommonModule } from "@angular/common"
import { FormsModule, ReactiveFormsModule } from "@angular/forms"

const modules = [
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSortModule,
  MatDialogModule,
  MatTableModule,
  MatCardModule
]

@NgModule({
  declarations: [
    PatientListComponent,
    PatientDetailComponent,
    AddPatientsComponent
  ],
  imports: [
    PatientsRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    modules
  ],
  entryComponents: [
    AddPatientsComponent
  ]
})
export class PatientModule {

}
