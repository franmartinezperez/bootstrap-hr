import { async, ComponentFixture, TestBed } from "@angular/core/testing"
import {
  MatButtonModule,
  MatSortModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatTableModule,
  MatCardModule
} from "@angular/material"
import { MatDialogRef, MatDialogModule } from "@angular/material/dialog"
import { AddPatientsComponent } from "./add-patients.component"
import { ReactiveFormsModule, FormsModule } from "@angular/forms"
import { PatientsService } from "src/app/services/patients.service"
import { MAT_DIALOG_DATA } from "@angular/material"
import { BrowserAnimationsModule } from "@angular/platform-browser/animations"

describe("AddPatientsComponent", () => {
  let component: AddPatientsComponent
  let fixture: ComponentFixture<AddPatientsComponent>

  const dialogMock = {
    updatePosition: () => { }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSortModule,
        MatDialogModule,
        MatTableModule,
        MatCardModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
      ],
      declarations: [AddPatientsComponent],
      providers: [
        PatientsService,
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: {} },
        { provide: MatDialogRef, useValue: dialogMock },
      ]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPatientsComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it("should create", () => {
    expect(component).toBeTruthy()
  })
})
