import { Component, Inject, OnInit } from "@angular/core"
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material"
import { FormBuilder, FormControl, Validators, FormGroup } from "@angular/forms"
import { Patient } from "src/app/models/patient"
import { PatientsService } from "src/app/services/patients.service"

@Component({
  selector: "app-dialog",
  templateUrl: "./add-patients.component.html",
  styleUrls: ["./add-patients.component.scss"],
})
export class AddPatientsComponent implements OnInit {

  // today date for datepicker
  today: Date = new Date()

  // form
  addPatientForm: FormGroup
  submitted = false

  constructor(
    public dialogRef: MatDialogRef<AddPatientsComponent>,
    private patientsService: PatientsService,
    @Inject(MAT_DIALOG_DATA) public data) {
  }

  ngOnInit() {
    this.dialogRef.updatePosition({ top: "0", right: "0" })

    // --- FORM ---
    this.addPatientForm = new FormGroup({
      name: new FormControl("", Validators.compose([
        Validators.required,
        Validators.maxLength(60)
      ])),
      surname: new FormControl("", Validators.compose([
        Validators.required,
        Validators.maxLength(60)
      ])),
      dateOfBirth: new FormControl(new Date(), Validators.compose([
        Validators.required
      ]))
    })
  }

  // check form errors
  public hasError = (controlName: string, errorName: string) => {
    return this.addPatientForm.controls[controlName].hasError(errorName)
  }

  // click outside dialog
  public onNoClick(): void {
    this.dialogRef.close()
  }

  public addPatient = (addPatientFormValue) => {
    // if no error on form then add patient
    if (this.addPatientForm.valid) {
      this.executeaddPatientAdd(addPatientFormValue)
    }
  }

  // add patient to local storage
  private executeaddPatientAdd = (addPatientFormValue) => {
    const patient: Patient = { identifier: 0, name: "", surname: "", birthDate: new Date(), numberOfRevisions: 0 }

    let lastPatient: Patient

    // save last patient to set identifier of the new one depending on the last
    lastPatient = this.patientsService.patients[this.patientsService.patients.length - 1]

    patient.identifier = lastPatient.identifier + 1
    patient.name = addPatientFormValue.name
    patient.surname = addPatientFormValue.surname
    patient.birthDate = addPatientFormValue.dateOfBirth
    patient.numberOfRevisions = Math.floor(Math.random() * 100) + 1

    this.patientsService.addPatient(patient)

    this.dialogRef.close()

  }

}
