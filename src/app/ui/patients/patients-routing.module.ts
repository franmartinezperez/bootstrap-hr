import { NgModule } from "@angular/core"
import { Routes, RouterModule } from "@angular/router"
import { Path } from "../../configs/path"
import { PatientDetailComponent } from "./patient-detail/patient-detail.component"
import { PatientListComponent } from "./patient-list/patient-list.component"

const routes: Routes = [
	{
		path: Path.SCREEN_PATIENTS_LIST_TABLE,
		component: PatientListComponent
	},
	{
		path: Path.SCREEN_PATIENTS_DETAIL,
		component: PatientDetailComponent
	},
	{
		path: Path.DEFAULT,
		redirectTo: Path.SCREEN_PATIENTS_LIST_TABLE,
		pathMatch: "full"
	}
]

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PatientsRoutingModule {
}
