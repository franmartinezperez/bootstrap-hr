import { async, ComponentFixture, TestBed } from "@angular/core/testing"
import {
  MatButtonModule,
  MatDialogModule,
  MatSortModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatTableModule,
  MatCardModule
} from "@angular/material"
import { PatientDetailComponent } from "./patient-detail.component"
import { FormsModule, ReactiveFormsModule } from "@angular/forms"
import { PatientsService } from "src/app/services/patients.service"
import { Router, RouterModule } from "@angular/router"
import { RouterTestingModule } from "@angular/router/testing"
import { BrowserAnimationsModule } from "@angular/platform-browser/animations"

describe("PacientDetailComponent", () => {
  let component: PatientDetailComponent
  let fixture: ComponentFixture<PatientDetailComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSortModule,
        MatDialogModule,
        MatTableModule,
        MatCardModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([]),
        BrowserAnimationsModule
      ],
      declarations: [PatientDetailComponent],
      providers: [
        PatientsService
      ]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientDetailComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it("should create", () => {
    expect(component).toBeTruthy()
  })
})
