import { Component, OnInit, ViewChild } from "@angular/core"
import { PatientsService } from "src/app/services/patients.service"
import { Patient } from "src/app/models/patient"
import { Router } from "@angular/router"
import { Path } from "src/app/configs/path"
import { Reviews } from "src/app/models/reviews"
import { FAKE_REVIEWS } from "src/app/mock/data"
import { MatTableDataSource, MatSort } from "@angular/material"

@Component({
  selector: "app-patient-detail",
  templateUrl: "./patient-detail.component.html",
  styleUrls: ["./patient-detail.component.scss"]
})
export class PatientDetailComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort

  patientSelected?: Patient
  patientBirth?: Date = new Date()
  patientName?: string
  patientSurname?: string
  patientIdentifier?: number

  displayedColumns: string[] = ["number", "reviewDate", "doctor", "diagnosis"]
  dataSource: MatTableDataSource<Reviews>

  constructor(
    private patientsService: PatientsService,
    private rout: Router
  ) { }

  ngOnInit() {
    this.loadData()
  }

  loadData() {
    this.patientSelected = JSON.parse(localStorage.getItem("selectedPatient"))
    if (this.patientSelected !== null) {
      this.patientBirth = this.patientSelected.birthDate
      this.patientName = this.patientSelected.name
      this.patientSurname = this.patientSelected.surname
      this.patientIdentifier = this.patientSelected.identifier
    }

    // table data
    this.dataSource = new MatTableDataSource<Reviews>(FAKE_REVIEWS)
    this.dataSource.sort = this.sort
  }

  onBackClick() {
    this.rout.navigateByUrl(Path.MODULE_PATIENTS + Path.BACKSLASH + Path.SCREEN_PATIENTS_LIST_TABLE)
  }

}
